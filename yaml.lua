-- YAML parser, requires internet connection, a json parser and html parser.

-- Include
include = include or function()
	local libraryPath = library:gsub(".", "/")
	local prefixes = {
		"/rom/apis/",
		"/rom/apis/command/",
		"/rom/apis/turtle/",
		"/boot/lib/",
		"/lib/",
		""
	}
	local export
	for i, prefix in ipairs(prefixes) do
		if fs.exists(prefix..libraryPath) then
			export = dofile(prefix..libraryPath)
		elseif fs.exists(prefix..libraryPath..".lua") then
			export = dofile(prefix..libraryPath)
		end
	end
	return export
end

-- Libraries
local json    = json    or include "libwww.json"
local libhttp = libhttp or include "libhttp"

-- Global data
local provider = "http://yaml-online-parser.appspot.com/"

-- Functions
local libyaml = {}
-- Parse YAML from string
function libyaml.parse(input)
	if not input then return false end
	if libhttp then
		local Provider = libhttp.HttpConnection.new(provider)
		Provider:addParameter("yaml", textutils.urlEncode(tostring(input)))
		Provider:addParameter("type", "json")
		local Response = Provider:get().readAll()
		if not Response then return false end
		return libjson.decode(Response)
	else
		local Response = http.get(Provider.."?yaml="..textutils.urlEncode(tostring(input)).."&type=json")
		if not response then return false end
		return libjson.decode(Response)
	end
end
-- Read YAML from file
function libyaml.parseFile(path)
	local file = fs.open(path)
	local data = libyaml.parse(file.readAll())
	file.close(); return data
end
