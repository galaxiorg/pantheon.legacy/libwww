# libwww
Set of parsing libraries for ComputerCraft, focused for web uses (JSON, XML, HTML, Markdown)
-----
### Contributing
Make a pull request with your own implementation and it will be reviewed

### License
There is no license for this repo. However, this doesn't mean they are free to
use. Check the conditions in the header of each file or ask the responsible user.

All the licenses are included in the documentation for each file (in docs/)

### Bugs
Contact [me](mailto:daelvn@gmail.com) or the mantainer of the file.

### Notes
This repository is unfinished and some files need changes to work completely in
ComputerCraft.

### Credits
[Paul Chakravarti](mailto:paulc@passtheaardvark.com) and [Manoel Campos da Silva Filho](http://about.me/manoelcampos) for [LuaXML](https://github.com/manoelcampos/LuaXML)

ElvishJerrico for json.lua

T. Kobayashi for html.lua

[Niklas Frykholm](mailto:niklas@frykholm.se) for markdown.lua

[Jonathan Stoler](https://github.com/jonstoler) for [lua-toml](https://github.com/jonstoler/lua-toml)
