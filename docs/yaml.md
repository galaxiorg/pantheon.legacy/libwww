# libwww
## How to use yaml.lua
---
### Previous information
This library requires a HTTP connection and a JSON parser, because there is no
pure Lua parser, so it uses a web service to parse it into JSON. yaml.lua will
try to use libhttp if loaded/installed, but it can fallback to the native Http API.

### Loading
`local yaml = dofile("path/to/yaml.lua")`

### Parsing
#### From a string
`local parsed = yaml.parse(text)`
#### From a file
`local parsedFile = yaml.parseFile(path)`
