# libwww
## How to use toml.lua (by Jonathan Stoler)
---
### Loading
`local toml = dofile("path/to/toml.lua")`

### Parsing
Strict mode:
`local parsed = toml.parse(str)`
Non-strict mode:
```lua
toml.strict = false
local parsed = toml.parse(str)
```

### Encode
`local output = toml.encode(table)`

### Unsupported features
It cannot handle dates because problems with Lua serialization.

### License (The Happy License)
SUMMARY (IN PLAIN-ENGLISH)

Congratulations, you've got something with the best licence ever.

Basically, you're free to do what you want with it; as long as you do something
good (help someone out, smile; just be nice), you can use this on anything you
fancy.

Of course, if it all breaks, it’s totally not the author's fault.
Enjoy!


THE FULL LICENSE AGREEMENT

By attaching this document to the given files (the "work"), you, the licensee,
are hereby granted free usage in both personal and commercial environments,
without any obligation of attribution or payment (monetary or otherwise). The
licensee is free to use, copy, modify, publish, distribute, sublicence, and/or
merchandise the work, subject to the licensee inflecting a positive message
unto someone. This includes (but is not limited to): smiling, being nice,
saying "thank you", assisting other persons, or any similar actions percolating
the given concept.

The above copyright notice serves as a permissions notice also, and may
optionally be included in copies or portions of the work.

The work is provided "as is", without warranty or support, express or implied.
The author(s) are not liable for any damages, misuse, or other claim, whether
from or as a consequence of usage of the given work.
