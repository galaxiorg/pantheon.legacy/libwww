# libwww
## How to use json.lua (by ElvishJerrico)
---
### Loading
`local json = dofile("path/to/json.lua")`

### Encoding
To encode without readability (not human-friendly)
`local encoded = json.encode(tbl)`
To encode user-friendly
`local prettyEncoded = json.encodePretty(tbl)`

### Decoding
`local decoded = json.decode(str)`
Or, if you want to decode from a file
`local decoded = json.decodeFromFile(str)`

### License
No license is included with the file.
