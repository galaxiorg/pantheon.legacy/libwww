# libwww
## How to use html.lua (by T. Kobayashi)
---
### Loading
`local html = dofile("path/to/html.lua")`

### Parsing
If the following input is given:

```
<html><body>
<p>
  Click <a href="http://example.com/">here!</a>
<p>
  Hello
</p>
</body></html>
```

Then, the parser produces the following table:

```
{
  _tag = "#document",
  _attr = {},
  {
    _tag = "html",
    _attr = {},
    {
      _tag = "body",
      _attr = {},
      "\n",
      {
        _tag = "p",
        _attr = {},
        "\n  Click ",
        {
          _tag = "a",
          _attr = {href = "http://example.com/"}
          "here!",
        },
        "\n",
      },
      {
        _tag = "p",
        _attr = {},
        "\n  Hello\n",
      },
      "\n",
    }
  }
}
```

### Usage
Parsing string:

`html.parsestr("<html></html>")`

### License
The file uses a standard MIT license


Copyright (c) 2007 T. Kobayashi

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
