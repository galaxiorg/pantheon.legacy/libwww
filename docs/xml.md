# libwww
## How to use xml.lua (by Manoel Campos da Silva Filho)
---
### Loading
`local xml = dofile("path/to/xml.lua")`

### Using
The parser provides a partially object-oriented API with
functionality split into tokeniser and handler components.

The handler instance is passed to the tokeniser and receives
callbacks for each XML element processed (if a suitable handler
function is defined). The API is conceptually similar to the
SAX API but implemented differently.

**Further information is defined inside xml.lua**

A handles (simpleTreeHandler) has been provided in the file (under xml.) for
ease of use, but you can use your own handlers (see xml.lua).

The following example teaches you how to use it with simpleTreeHandler:

```lua
Parser = xml.create(xml.simpleTreeHandler)
local parsedData = Parser:parse(myXml)
```

### License
The MIT License (MIT)

Copyright (c) 2016 Manoel Campos da Silva Filho

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
